package com.example.halodocc.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.halodocc.model.Hit

@Dao
interface HitDao {

    @Query("SELECT * FROM Hits")
    fun getHits(): LiveData<List<Hit>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(plants: List<Hit>)

}