package com.example.halodocc.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.halodocc.R
import com.example.halodocc.model.Hit
import com.example.halodocc.view.adapter.SearchHnAdapter
import kotlinx.android.synthetic.main.layout_search_fragment.*

class SearchFragment : Fragment(), Observer<List<Hit>> {

    private val viewModel by lazy{
        ViewModelProviders.of(this).get(SearchViewModel::class.java)
    }

    private val searchAdapter by lazy {
        SearchHnAdapter(CallBack())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_search_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.getSearchResultFor(activity as Context)
                .observe(viewLifecycleOwner,this)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = searchAdapter
        }
    }

    override fun onChanged(pagedList:List<Hit>?) {
        searchAdapter.submitList(pagedList)
    }

    class CallBack : DiffUtil.ItemCallback<Hit>() {
        override fun areItemsTheSame(oldItem: Hit, newItem: Hit): Boolean {
            return oldItem.storyId == newItem.storyId
        }

        override fun areContentsTheSame(oldItem: Hit, newItem: Hit): Boolean {
            return oldItem.title == newItem.title
        }
    }

}