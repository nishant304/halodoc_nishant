package com.example.halodocc.view

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.halodocc.db.AppDataBase
import com.example.halodocc.db.HitDao
import com.example.halodocc.model.Hit
import com.example.halodocc.repo.SearchDataSource
import com.example.halodocc.repo.SearchDataSourceFactory
import com.example.halodocc.repo.SearchRepo

class SearchViewModel : ViewModel() {

    fun getSearchResultFor(context: Context) : LiveData<List<Hit>>{
        val repo : SearchRepo = SearchRepo(AppDataBase.getInstance(context).hittDao())
        return repo.searchNews()
    }

}