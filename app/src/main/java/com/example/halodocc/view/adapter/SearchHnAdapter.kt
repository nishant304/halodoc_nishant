package com.example.halodocc.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.halodocc.R
import com.example.halodocc.model.Hit
import kotlinx.android.synthetic.main.layout_search_hn_item.view.*

class SearchHnAdapter(itemCallBack : DiffUtil.ItemCallback<Hit>) : ListAdapter<Hit, ItemHNHolder>(itemCallBack){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHNHolder {
        return ItemHNHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_search_hn_item,parent,false))
    }

    override fun onBindViewHolder(holder: ItemHNHolder, position: Int) {
        holder.itemView.title.setText(getItem(position)?.title)
    }

}
