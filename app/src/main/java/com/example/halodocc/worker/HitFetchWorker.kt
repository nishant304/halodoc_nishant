package com.example.halodocc.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.halodocc.client.Client
import com.example.halodocc.client.HNService
import com.example.halodocc.db.AppDataBase
import com.example.halodocc.model.Hit
import com.example.halodocc.model.SearchResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HitFetchWorker(context:Context,param:WorkerParameters) : CoroutineWorker(context,param) {

    private val hnService = Client.getClient().retrofitClient.create(HNService::class.java)

    override suspend fun doWork(): Result {
       val response: Response<SearchResponse> =  hnService.search("").execute()
        if(response.isSuccessful && response != null){
            response.body()?.hits?.let { AppDataBase.getInstance(context = applicationContext).hittDao().insertAll(it) }
        }
        return Result.success()
    }

}