package com.example.halodocc.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.List;

import com.example.halodocc.model.HighlightResult;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@Entity(tableName = "Hits")
public class Hit {

    @SerializedName("title")
    @Expose
    private String title;


    @PrimaryKey
    @SerializedName("objectID")
    @Expose
    private int storyId;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getStoryId() {
        return storyId;
    }

    public void setStoryId(int storyId) {
        this.storyId = storyId;
    }


}