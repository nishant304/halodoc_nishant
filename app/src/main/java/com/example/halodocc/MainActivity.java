package com.example.halodocc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.halodocc.view.SearchFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(savedInstanceState == null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.root, new SearchFragment())
                    .commit();
        }
    }

}
