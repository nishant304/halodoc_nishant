package com.example.halodocc.client;

import com.google.gson.GsonBuilder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class Client {

    private static Client client = new Client();
    private final Retrofit retrofitClient;

    private Client(){
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(0, TimeUnit.MILLISECONDS)
                .readTimeout(0, TimeUnit.MILLISECONDS)
                .writeTimeout(0, TimeUnit.MILLISECONDS)
                .build();


        retrofitClient = new Retrofit.Builder()
                .baseUrl("https://hn.algolia.com")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build();

    }


    public static Client getClient() {
        return client;
    }

    public Retrofit getRetrofitClient() {
        return retrofitClient;
    }
}
