package com.example.halodocc.client;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import com.example.halodocc.model.SearchResponse;

public interface HNService {

    @GET("/api/v1/search/")
    Call<SearchResponse> search(@Query("query") String query);

}
