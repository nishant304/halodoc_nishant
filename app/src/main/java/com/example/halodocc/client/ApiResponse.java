package com.example.halodocc.client;

import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;

public class ApiResponse<T> {

    private T data;

    private int code;

    private String errorMessage;

    public ApiResponse(Response<T> response) {
        this.code = response.code();

        if (response.isSuccessful()) {
            this.data = response.body();
        } else {
            StringBuilder message = new StringBuilder();
            try {
                if (response.errorBody() != null) {
                    String error = response.errorBody().string();
                    JSONArray errorsJson = new JSONObject(error).getJSONArray("errors");
                    for (int i = 0; i < errorsJson.length(); i++) {
                        message.append(errorsJson.getString(i));
                        message.append(" ");
                    }
                }
            } catch (Exception ignored) {
            }
            if (message.length() == 0) {
                message = new StringBuilder("Something went wrong");
            }
            errorMessage = message.toString();
        }
    }

    public ApiResponse(Throwable throwable) {
        code = 500;
        if (throwable instanceof HttpException) {
            errorMessage = "Something went wrong";
        } else if (throwable instanceof IOException) {
            this.errorMessage = "No Internet";
        } else {
            errorMessage = "Something went wrong";
        }
    }

    public boolean isSuccess() {
        return code >= 200 && code < 300;
    }

    public T response() {
        return data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public T getResponse() {
        return data;
    }

    private String getErrorMessage(ResponseBody responseBody) {
        try {
            JSONObject jsonObject = new JSONObject(responseBody.string());
            return jsonObject.getString("message");
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
