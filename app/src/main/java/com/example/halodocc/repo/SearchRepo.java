package com.example.halodocc.repo;

import androidx.lifecycle.LiveData;

import com.example.halodocc.db.HitDao;
import com.example.halodocc.model.Hit;

import java.util.List;

public class SearchRepo {

    private final HitDao hitDao;

    public SearchRepo(HitDao hitDao){
        this.hitDao = hitDao;
    }

    public LiveData<List<Hit>> searchNews(){
        return hitDao.getHits();
    }

}
