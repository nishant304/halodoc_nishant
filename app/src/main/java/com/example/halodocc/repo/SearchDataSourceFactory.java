package com.example.halodocc.repo;

import androidx.paging.DataSource;

import com.example.halodocc.model.Hit;

public class SearchDataSourceFactory extends DataSource.Factory<Integer, Hit> {

    private SearchDataSource searchDataSource;

    public SearchDataSourceFactory(){
        searchDataSource = new SearchDataSource();
    }

    @Override
    public DataSource<Integer, Hit> create() {
        return searchDataSource;
    }

}
