package com.example.halodocc.repo;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.example.halodocc.client.Client;
import com.example.halodocc.client.HNService;
import com.example.halodocc.model.Hit;
import com.example.halodocc.model.SearchResponse;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchDataSource extends PageKeyedDataSource<Integer, Hit> {

    private HNService hnService = Client.getClient().getRetrofitClient().create(HNService.class);

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, Hit> callback) {
        Call<SearchResponse> call =  hnService.search("");
        try {
            Response<SearchResponse> response = call.execute();
            if(response.isSuccessful() && response.body() != null) {
                callback.onResult(response.body().getHits(), null, 1);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Hit> callback) {

    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, Hit> callback) {

    }

}
